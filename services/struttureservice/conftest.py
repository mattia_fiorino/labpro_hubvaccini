import pytest
import dockerfile
import os
import logging
from unittest import mock
import mongomock
import datetime
import importlib.util
from uuid import uuid4
from werkzeug.security import generate_password_hash


@pytest.fixture
def struttureserviceApp():
    # Import the whole app.py without stardard python import method
    # So the test can be executed without worrying where pytest is launched
    with mock.patch.dict(os.environ, env_vars()):
        struttureservice_app_spec = importlib.util.spec_from_file_location(
            'utenteserviceApp', './services/struttureservice/app.py')
        struttureservice_app = importlib.util.module_from_spec(struttureservice_app_spec)
        struttureservice_app_spec.loader.exec_module(struttureservice_app)
        yield struttureservice_app

@pytest.fixture
def Struttura():
    # Import the whole Struttura.py model without stardard python import method
    Struttura_spec = importlib.util.spec_from_file_location(
        'Struttura', './services/struttureservice/sources/models/Struttura.py')
    Struttura = importlib.util.module_from_spec(Struttura_spec)
    Struttura_spec.loader.exec_module(Struttura)

    # Returns the Struttura model class
    return Struttura.Struttura

@pytest.fixture
def Slot():
    # Import the whole Slot.py model without stardard python import method
    Slot_spec = importlib.util.spec_from_file_location(
        'Slot', './services/struttureservice/sources/models/Slot.py')
    Slot = importlib.util.module_from_spec(Slot_spec)
    Slot_spec.loader.exec_module(Slot)

    # Returns the Struttura model class
    return Slot.Slot


@pytest.fixture
def tokenBlacklist():
    # Import the whole tokenBlacklist.py model without stardard python import method
    tokenBlacklist_spec = importlib.util.spec_from_file_location(
        'tokenBlacklist', './services/struttureservice/sources/models/tokenBlacklist.py')
    tokenBlacklist = importlib.util.module_from_spec(tokenBlacklist_spec)
    tokenBlacklist_spec.loader.exec_module(tokenBlacklist)

    # Returns the tokenBlacklist model class
    return tokenBlacklist.tokenBlacklist


@pytest.fixture
def struttureserviceClient(struttureserviceApp, Struttura):
    # Import configuration script
    config_spec = importlib.util.spec_from_file_location(
        'struttureserviceApp', './services/struttureservice/config.py')
    config = importlib.util.module_from_spec(config_spec)
    config_spec.loader.exec_module(config)

    # Create Flask app with mongomock
    app = struttureserviceApp.create_app(config.testConfigs(isMock=True))

    # Run the test_client
    test_client = app.test_client()

    return test_client

# The Flask app is now started with appropriate configuration for testing
# No need for env variables moking anymore
def env_vars() -> dict:
    df_content = (x for x in dockerfile.parse_file(
         './services/struttureservice/Dockerfile') if x.cmd == 'env')
    envs = {}
    for env in df_content:
         envs[env.value[0]] = env.value[1].strip('\'')
    return envs


# Usefull now because the old utenteservices endpoint are no longer used
# def defineSomePrenotazioni() -> dict:
#     prenotazioni = {}
#     prenotazioni['prenotazioni'] = []
#     prenotazioni['prenotazioni'].append({
#         'nome': 'Francesco Colombo',
#         'cf': 'CLMFNC70M05G535O',
#         'dataprenotazione': str(datetime.date(2021, 3, 1)),
#         'allergie': 'Morfina'
#     })
#     prenotazioni['prenotazioni'].append({
#         'nome': 'Alice Ricci',
#         'cf': ' LCARCC61S46G337H',
#         'dataprenotazione': str(datetime.date(2021, 3, 11)),
#         'allergie': ''
#     })
#     prenotazioni['prenotazioni'].append({
#         'nome': 'Greta Martini',
#         'cf': 'GRTMTN88D44A662C',
#         'dataprenotazione': str(datetime.date(2021, 2, 27)),
#         'allergie': 'Cefalosporine'
#     })
#     prenotazioni['prenotazioni'].append({
#         'nome': 'Riccardo Villa',
#         'cf': 'RCCVLL64C08D205L',
#         'dataprenotazione': str(datetime.date(2021, 3, 16)),
#         'allergie': ''
#     })
#     return prenotazioni
