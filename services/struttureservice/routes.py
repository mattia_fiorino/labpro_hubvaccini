def register_routes(api, app, root='api'):
    # Import controllers api
    from sources.controllers.struttura_controller import api as strutture_api, BASE_ROUTE as strutture_base_route
    from sources.controllers.slot_controller import api as slot_api, BASE_ROUTE as slot_base_route

    # Add routes
    api.add_namespace(strutture_api, path=f'/{root}/{strutture_base_route}')
    api.add_namespace(slot_api, path=f'/{root}/{slot_base_route}')