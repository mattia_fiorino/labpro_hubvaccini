from flask import request, jsonify, Flask, make_response
from flask_accepts import accepts, responds
from flask_restplus import Namespace, Resource
from flask.wrappers import Response
from mongoengine.queryset.visitor import Q
import re
import requests
from datetime import datetime

from ..models.Slot import Slot
from ..models.Struttura import Struttura

api = Namespace('Slot', description='Single namespace, single entity')  # noqa
BASE_ROUTE = 'Slot'


@api.route('/List/<strutId>')
@api.param('strutId', 'Struttura database ID')
class ListResource(Resource):
    '''List'''

    # @responds(schema=ListDateSchema)
    def get(self, strutId) -> Response:
        '''Ottenere l'elenco degli slot dato un id struttura'''
        try:
            struttura = Struttura.objects(pubId=strutId).get()
        except:
            response = jsonify(dict(Errore='Struttura non trovata'))
            response.headers.add('Access-Control-Allow-Origin', '*')
            return make_response(response, 404)

        slots = Slot.objects(Q(struttura=struttura) & Q(prenotato=False)).as_pymongo()

        result = {"ListDateResult": []}
        for s in slots:
            result["ListDateResult"].append(s)

        response = jsonify(result)
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response


@api.route('/List/<strutId>/<data>')
@api.param('strutId', 'Struttura database ID')
@api.param('data', 'Data slot prenotazione')
class ListDataResource(Resource):
    '''List'''

    # @responds(schema=ListDateSchema)
    def get(self, strutId, data) -> Response:
        '''Ottenere l'elenco degli slot dato un id struttura e la data di appuntamento'''
        if re.match("[0-9]{4}-[0-9]{2}-[0-9]{2}", data):
            split = data.split("-")

            # datetime(year, month, day, hour, minute, second, microsecond)
            begin = datetime(int(split[0]), int(split[1]), int(split[2]), 0, 0, 0, 0)
            end = datetime(int(split[0]), int(split[1]), int(split[2]), 23, 59, 59, 0)
            try:
                struttura = Struttura.objects(pubId=strutId).get()
            except:
                response = jsonify(dict(Errore='Struttura non trovata'))
                response.headers.add('Access-Control-Allow-Origin', '*')
                return make_response(response, 404)

            slots = Slot.objects(
                Q(struttura=struttura) & Q(prenotato=False) & Q(data__gte=begin) & Q(data__lte=end)).as_pymongo()

            result = {"ListDateResult": []}
            for s in slots:
                result["ListDateResult"].append(s)

            response = jsonify(result)
            response.headers.add('Access-Control-Allow-Origin', '*')
            return response
        else:
            response = jsonify(dict(Errore='Formato della data errato'))
            response.headers.add('Access-Control-Allow-Origin', '*')
            return make_response(response, 400)


@api.route('/Book/<slotId>')
@api.param('slotId', 'Slot database ID')
class BookResource(Resource):
    '''Book'''

    # @responds(schema=ConfirmPrenotazioneSchema)
    def post(self, slotId) -> Response:
        '''Prenotare uno slot dato l'id'''
        payload = request.json

        if payload and 'userid' in payload:
            try:
                slot = Slot.objects(pubId=slotId).get()
            except:
                response = jsonify(dict(Errore='Slot non trovato'))
                response.headers.add('Access-Control-Allow-Origin', '*')
                return make_response(response, 404)

            if not slot.prenotato:
                slot.userId = str(payload['userid'])
                slot.prenotato = True
                slot.save()
            else:
                response = jsonify(dict(Errore='Slot richiesto già occupato'))
                response.headers.add('Access-Control-Allow-Origin', '*')
                return response

            response = jsonify(
                dict(_id=slot.pubId, struttura=slot.struttura.pubId, data=slot.data, prenotato=slot.prenotato,
                     userId=slot.userId, cancellato=slot.cancellato))
            response.headers.add('Access-Control-Allow-Origin', '*')
            return response
        else:
            response = jsonify(dict(Errore='L\'id dell\'utente è obbligatorio'))
            response.headers.add('Access-Control-Allow-Origin', '*')
            return make_response(response, 400)
