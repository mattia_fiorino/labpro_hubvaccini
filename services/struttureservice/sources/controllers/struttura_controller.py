from flask import request, jsonify, Flask
from flask_accepts import accepts, responds
from flask_restplus import Namespace, Resource
from flask.wrappers import Response
from pymongo import MongoClient
import os
import requests
import datetime
from typing import List

from ..models.Struttura import Struttura

api = Namespace('Strutture', description='Single namespace, single entity')  # noqa
BASE_ROUTE = 'Strutture'

@api.route('/ListAll')
class ListAllResource(Resource):
    '''ListAll'''

    #@token_required
    #@responds(schema=ListStruttureSchema, many=True)
    def get(self) -> Response:
        '''Ottenere la lista di tutte le strutture a sistema'''
        strutture = Struttura.objects(cancellato=False).as_pymongo()
        result = {"ListStruttureResult": []}
        for s in strutture:
            result["ListStruttureResult"].append(s)

        response = jsonify(result)
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response
