from functools import wraps

import jwt
from flask import jsonify, request, make_response
from flask import current_app as app


def token_required(f):
    @wraps(f)
    def decorated(self, *args, **kwargs):
        token = None

        auth_header = request.headers.get('Authorization')

        if auth_header:
            token = auth_header.split(" ")[1]

        if not token:
            response = jsonify(dict(Errore='Token is invalid!'))
            response.headers.add('Access-Control-Allow-Origin', '*')
            response.status_code = 401
            return response

        try:
            #data = jwt.decode(token, str(app.config['SECRET_KEY']))
            self.token = token

        except Exception as e:
            print(e)
            response = jsonify(dict(Errore='Token is invalid!'))
            response.headers.add('Access-Control-Allow-Origin', '*')
            response.status_code = 401
            return response

        return f(self, *args, **kwargs)
    return decorated
