import mongoengine as me
import uuid

from .Struttura import Struttura


class Slot(me.Document):
    pubId = me.UUIDField(default=lambda: str(uuid.uuid4()), primary_key=True, unique=True)
    struttura = me.ReferenceField('Struttura')
    data = me.DateTimeField(required=True)
    prenotato = me.BooleanField(required=True, default=False)
    userId = me.UUIDField()
    cancellato = me.BooleanField(required=True, default=False)
