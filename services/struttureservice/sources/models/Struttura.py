import mongoengine as me
import uuid

class Struttura(me.Document):
    pubId = me.UUIDField(default=lambda: str(uuid.uuid4()), primary_key=True, unique=True)
    ragioneSociale = me.StringField(required=True, unique=True)
    piva = me.StringField(required=True, unique=True)
    via = me.StringField(required=True, unique=True)
    cap = me.StringField(required=True, unique=True)
    comune = me.StringField(required=True, unique=True)
    provincia = me.StringField(required=True, unique=True)
    regione = me.StringField(required=True, unique=True)
    telefono = me.StringField()
    email = me.StringField()
    cancellato = me.BooleanField(required=True, default=False)
