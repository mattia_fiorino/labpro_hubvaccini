import os

from flask import Flask
from flask_mongoengine import MongoEngine


class prodConfigs():
    def config_mongodb(app: Flask) -> MongoEngine:

        MONGO_HOST = os.environ["MONGODBCONT_HOST"]
        MONGO_USERNAME = os.environ["MONGODBCONT_USER"]
        MONGO_PASSWORD = os.environ["MONGODBCONT_PWD"]
        MONGO_PORT = '27017'
        MONGO_DB = 'dbUtente'
        connection_string = f"mongodb://{MONGO_USERNAME}:{MONGO_PASSWORD}@{MONGO_HOST}:{MONGO_PORT}/{MONGO_DB}"
        url_params = '?authSource=admin&readPreference=primary&ssl=false'

        app.config['MONGODB_SETTINGS'] = {
            'host': connection_string + url_params,
            'connect': False,
        }
        return MongoEngine(app)

    def config_app(app: Flask):
        app.config['SECRET_KEY'] = os.environ['SECRET_KEY']


class testConfigs():

    connection_engine = ''

    def __init__(self, isMock):
        self.connection_engine = 'mongomock' if isMock else 'mongodb'

    def config_mongodb(self, app: Flask) -> MongoEngine:

        MONGO_HOST = 'localhost'
        MONGO_PORT = '27017'
        MONGO_DB = 'dbUtente'
        MONGO_USERNAME = 'root'
        MONGO_PASSWORD = 'root_password'
        connection_string = f"{self.connection_engine}://{MONGO_USERNAME}:{MONGO_PASSWORD}@{MONGO_HOST}:{MONGO_PORT}/{MONGO_DB}"
        url_params = '?authSource=admin&readPreference=primary&ssl=false'
        app.config['MONGODB_SETTINGS'] = {
            'host': connection_string + url_params,
            'connect': False,
        }

        return MongoEngine(app)

    def config_app(self, app: Flask):
        app.config['SECRET_KEY'] = 'labpro_hubvaccini'
