import argparse
import json
import os
import uuid
from base64 import b64encode
from datetime import datetime
from uuid import uuid4

from flask import Flask, jsonify, request
from flask_restplus import Api
from flask_cors import CORS

from config import prodConfigs, testConfigs
from routes import register_routes
from sources.models.Slot import Slot
from sources.models.Struttura import Struttura

def create_app(conf) -> Flask:

    # Create the app
    app = Flask(__name__)

    # Setup db
    db = conf.config_mongodb(app)

    # Create RESTplus API
    api = Api(app)

    # Configure the app
    conf.config_app(app)

    # Setup the routes
    register_routes(api, app)

    CORS(app, resources={r'/*': {'origins': '*'}})

    return app


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-t', '--Test', action='store_true')

    is_debug = parser.parse_args()

    if is_debug.Test:
        app = create_app(testConfigs(True))
        idStruttura = str(uuid.uuid4())
        idSlot = str(uuid.uuid4())
        idSlot2 = str(uuid.uuid4())
        dataSlot = datetime(2021, 4, 20, 12, 0, 0, 0)
        dataSlot2 = datetime(2021, 4, 20, 12, 30, 0, 0)
        struttura = Struttura(pubId=idStruttura,
                              ragioneSociale="Ospedale sant'Anna di Como",
                              piva="asiud786das87dgasd",
                              via="via Roma 14",
                              cap="22100",
                              comune="Como",
                              provincia="Como",
                              regione="Lombardia",
                              telefono="0319988765",
                              email="centroassistenza@asstlariana.com")
        struttura.save()
        Slot(pubId=idSlot,
             struttura=struttura,
             data=dataSlot
             ).save()
        Slot(pubId=idSlot2,
             struttura=struttura,
             data=dataSlot2
             ).save()
        app.run(host="0.0.0.0", port=8081, debug=False)
    else:
        app = create_app(prodConfigs)
        app.run(host="0.0.0.0", port=8081)
