import json
import uuid
from datetime import datetime
from unittest.mock import patch

# from flask.testing import FlaskClient
from flask import Flask

from services.struttureservice.sources.models.Struttura import Struttura
from services.struttureservice.sources.models.Slot import Slot
from services.struttureservice.conftest import struttureserviceClient


class TestListResource:
    def test_get(self, struttureserviceClient):  # noqa
        idStruttura = str(uuid.uuid4())
        idSlot = str(uuid.uuid4())
        dataSlot = datetime(2021, 4, 20, 12, 0, 0, 0)
        struttura = Struttura(pubId=idStruttura,
                              ragioneSociale="Ospedale sant'Anna di Como",
                              piva="asiud786das87dgasd",
                              via="via Roma 14",
                              cap="22100",
                              comune="Como",
                              provincia="Como",
                              regione="Lombardia",
                              telefono="0319988765",
                              email="centroassistenza@asstlariana.com")
        struttura.save()
        Slot(pubId=idSlot,
             struttura=struttura.to_dbref(),
             data=dataSlot
             ).save()
        results = struttureserviceClient.get('/api/Slot/List/' + idStruttura,
                                             headers=dict(Authorization='Bearer PRVPRV00C34O234G'),
                                             follow_redirects=True).get_json()
        assert results == json.loads('{"ListDateResult": [{"_id": "' + idSlot + '", "cancellato": false, "data": "Tue, 20 Apr 2021 12:00:00 GMT", "prenotato": false, "struttura": "' + idStruttura + '"}]}')

        results = struttureserviceClient.get('/api/Slot/List/identifica_struttura',
                                             headers=dict(Authorization='Bearer PRVPRV00C34O234G'),
                                             follow_redirects=True)
        assert results.status_code == 404
        assert results.get_json() == json.loads('{"Errore": "Struttura non trovata"}')


class TestListDateResource:
    def test_get(self, struttureserviceClient):  # noqa
        struttura = Struttura.objects.first()
        slot = Slot.objects.first()
        results = struttureserviceClient.get('/api/Slot/List/' + str(struttura.pubId) + '/2021-04-20',
                                             headers=dict(Authorization='Bearer PRVPRV00C34O234G'),
                                             follow_redirects=True).get_json()

        assert results == json.loads('{"ListDateResult": [{"_id": "' + str(slot.pubId) + '", "cancellato": false, "data": "Tue, 20 Apr 2021 12:00:00 GMT", "prenotato": false, "struttura": "' + str(struttura.pubId) + '"}]}')

        results = struttureserviceClient.get('/api/Slot/List/' + str(struttura.pubId) + '/20210420',
                                             headers=dict(Authorization='Bearer PRVPRV00C34O234G'),
                                             follow_redirects=True)
        assert results.status_code == 400
        assert results.get_json() == json.loads('{"Errore": "Formato della data errato"}')

        results = struttureserviceClient.get('/api/Slot/List/identifica_struttura/2021-04-20',
                                             headers=dict(Authorization='Bearer PRVPRV00C34O234G'),
                                             follow_redirects=True)
        assert results.status_code == 404
        assert results.get_json() == json.loads('{"Errore": "Struttura non trovata"}')

        results = struttureserviceClient.get('/api/Slot/List/' + str(struttura.pubId) + '/2021-05-20',
                                             headers=dict(Authorization='Bearer PRVPRV00C34O234G'),
                                             follow_redirects=True)
        assert results.get_json() == json.loads('{"ListDateResult": []}')


class TestBookResource:
    def test_post(self, struttureserviceClient):  # noqa
        struttura = Struttura.objects.first()
        slot = Slot.objects.first()
        userId = str(uuid.uuid4())
        results = struttureserviceClient.post('/api/Slot/Book/' + str(slot.pubId),
                                              headers=dict(Authorization='Bearer PRVPRV00C34O234G'),
                                              json={"userid": userId},
                                              follow_redirects=True).get_json()

        assert results == json.loads('{"_id": "' + str(slot.pubId) + '", "cancellato": false, "data": "Tue, 20 Apr 2021 12:00:00 GMT", "prenotato": true, "userId": "' + userId + '", "struttura": "' + str(struttura.pubId) + '"}')

        results = struttureserviceClient.post('/api/Slot/Book/' + str(slot.pubId),
                                              headers=dict(Authorization='Bearer PRVPRV00C34O234G'),
                                              json={"userid": userId},
                                              follow_redirects=True)

        assert results.get_json() == json.loads('{"Errore": "Slot richiesto già occupato"}')

        results = struttureserviceClient.post('/api/Slot/Book/' + str(slot.pubId),
                                              headers=dict(Authorization='Bearer PRVPRV00C34O234G'),
                                              json={"userid": userId},
                                              follow_redirects=True)

        assert results.get_json() == json.loads('{"Errore": "Slot richiesto già occupato"}')

        results = struttureserviceClient.post('/api/Slot/Book/id_slot',
                                              headers=dict(Authorization='Bearer PRVPRV00C34O234G'),
                                              follow_redirects=True)
        assert results.status_code == 400
        assert results.get_json() == json.loads('{"Errore": "L\'id dell\'utente è obbligatorio"}')
