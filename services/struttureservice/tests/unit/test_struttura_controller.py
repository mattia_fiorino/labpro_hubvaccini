import json
import uuid
from unittest.mock import patch

# from flask.testing import FlaskClient
from flask import Flask

from services.struttureservice.sources.models.Struttura import Struttura
from services.struttureservice.conftest import struttureserviceClient



class TestListAllResource:
    def test_get(self, struttureserviceClient):  # noqa
        struttura = Struttura.objects.first()
        results = struttureserviceClient.get('/api/Strutture/ListAll',
                                             headers=dict(Authorization='Bearer PRVPRV00C34O234G'),
                                             follow_redirects=True).get_json()

        assert results == json.loads('{"ListStruttureResult":[{"_id":"'+str(struttura.pubId)+'","ragioneSociale":"Ospedale sant\'Anna di Como",'
            '"piva":"asiud786das87dgasd","via":"via Roma 14","cap":"22100","comune":"Como","provincia":"Como",'
            '"regione":"Lombardia","telefono":"0319988765","email":"centroassistenza@asstlariana.com","cancellato":false}]}')

