import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './index.css';
import Header from './components/Header/Header';
import HeaderRegistrazione from './components/Header/Header2';
import HeaderDati from './components/Header/Header3';
import HeaderFaq from './components/Header/Header4';
import HeaderRicRec from './components/Header/Header5';
import HeaderRec from './components/Header/Header6';
import HeaderPreno from './components/Header/Header7';
import Corpo from './components/MainTemplate/MainTemplate';
import Footer from './components/Footer/Footer';
import NavBar from './components/NavBar/NavBar';
import reportWebVitals from './reportWebVitals';
import Registrazione from "./views/registrazione/registrazione";
import DatiNazionali from "./views/DatiNazionali/DatiNazionali";
import Recupero from "./views/Recupero/Recupero";
import RichiestaRecupero from "./views/Recupero/RichiestaRecupero";
import FAQ from "./views/FAQ/faq";
import Prenotazioni from "./views/Prenotazioni/Prenotazioni";
import Contatti from './components/contacts/contatti';
import LoginModal from './components/Auth/LoginModal'
import { NavLink } from 'reactstrap';

const nav = [
  { url: "/registrazione", text: "Registrazioni", exact: true, position: "left" },
  { url: "/faq", text: "FAQ", exact: true, position: "left" },
  { url: "/datinazionali", text: "Dati nazionali", exact: true, position: "left" },
  { url: "/prenotazione", text: "Prenota vaccino", exact: true, position: "left" },
  { component: <NavLink href="#contatti">Contatti</NavLink>, position: "left"},
  { component: <LoginModal buttonLabel={"Login"} /> , position: "right"}
]

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <NavBar navItems={nav}>
      </NavBar>
      <Switch>
        <Route exact path="/" >
          <Header />
          <Corpo />
        </Route>
        <Route exact path="/registrazione">
          <HeaderRegistrazione />
          <Registrazione />
        </Route>
        <Route exact path="/faq" >
          <HeaderFaq />
          <FAQ />
        </Route>
        <Route exact path="/datinazionali">
          <HeaderDati />
          <DatiNazionali />
        </Route>
        <Route exact path="/prenotazione">
          <HeaderPreno />
          <Prenotazioni/>
        </Route>
        <Route exact path="/richiesta_recupero_password">
          <HeaderRicRec />
          <RichiestaRecupero />
        </Route>
        <Route exact path="/recupero_password">
          <HeaderRec />
          <Recupero />
        </Route>
      </Switch>
      <Contatti />
      <Footer />
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
