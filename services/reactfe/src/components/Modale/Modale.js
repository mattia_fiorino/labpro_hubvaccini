import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, ModalTitle} from 'reactstrap';
import RegistrationBox from "../RegistrationBox/RegistrationBox";


function Modale(props) {


  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

    return (
    <>
      <Button variant="primary" onClick={handleShow}>
        Launch demo modal
      </Button>

      <Modal isOpen={show}>
        <RegistrationBox/>
        <ModalFooter>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleClose}>
            Save Changes
          </Button>
        </ModalFooter>
      </Modal>
    </>

    );
}

export default Modale;
