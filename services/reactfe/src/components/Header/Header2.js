function Header_registrazione() {
    return (
      <header class="masthead_registrazione">
        <div class="container d-flex h-100 align-items-center">
          <div class="mx-auto text-center">
            <h1 class="mx-auto my-0 text-uppercase">Registrati per prendere appuntamento</h1>
            <h2 class="text-white-50 mx-auto mt-2 mb-5"></h2>
            <a class="btn btn-primary js-scroll-trigger" href="#about">Inizia</a>
          </div>
        </div>
      </header>
    );
  }
  
  export default Header_registrazione;
  