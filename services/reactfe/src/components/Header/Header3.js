function Header_dati() {
    return (
      <header class="masthead_dati">
        <div class="container d-flex h-100 align-items-center">
          <div class="mx-auto text-center">
            <h1 class="mx-auto my-0 text-uppercase">Dati Nazionali</h1>
            <h2 class="text-white-50 mx-auto mt-2 mb-5">Aggiornati in tempo reale</h2>
            <a class="btn btn-primary js-scroll-trigger" href="#about">Controlla</a>
          </div>
        </div>
      </header>
    );
  }
  
  export default Header_dati;
  