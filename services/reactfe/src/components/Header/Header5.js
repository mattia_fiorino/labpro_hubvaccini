function Header_reset() {
    return (
      <header class="masthead_reset">
        <div class="container d-flex h-100 align-items-center">
          <div class="mx-auto text-center">
            <h1 class="mx-auto my-0 text-uppercase">Reset Password</h1>
            <h2 class="text-white-50 mx-auto mt-2 mb-5">Resettiamo la password</h2>
          </div>
        </div>
      </header>
    );
  }
  
  export default Header_reset;