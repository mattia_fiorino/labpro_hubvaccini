function Header() {

    return (
      <header class="masthead">
        <div class="container d-flex h-100 align-items-center">
          <div class="mx-auto text-center">
            <h1 class="mx-auto my-0 text-uppercase">Portale prenotazione Vaccini Covid </h1>
            <h2 class="text-white-50 mx-auto mt-2 mb-5">Applicazione per Laboratorio Progettazione</h2>
            <a class="btn btn-primary js-scroll-trigger" href="#about">Come Funziona</a>
          </div>
        </div>
      </header>
    );
  }
  
  export default Header;
  