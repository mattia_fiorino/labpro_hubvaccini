function Contatti() {
    return (
      <section class="contact-section bg-black" id="contatti">
              <div class="container">
                  <div class="row">
                      
                      <div class="col-md-6 mb-3 mb-md-0">
                          <div class="card py-4 h-100">
                              <div class="card-body text-center">
                                  <i class="fas fa-envelope text-primary mb-2"></i>
                                  <h4 class="text-uppercase m-0">Email</h4>
                                  <hr class="my-4" />
                                  <div class="small text-black-50"><a href="#!">info@prenotazioni-lombardia.it</a></div>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-6 mb-3 mb-md-0">
                          <div class="card py-4 h-100">
                              <div class="card-body text-center">
                                  <i class="fas fa-mobile-alt text-primary mb-2"></i>
                                  <h4 class="text-uppercase m-0">Telefono</h4>
                                  <hr class="my-4" />
                                  <div class="small text-black-50">+39 02 0001122</div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </section>
    );
  }
  
  export default Contatti;
  