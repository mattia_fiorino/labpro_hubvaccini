import React, { useState } from 'react';
import { NavLink as RouterLink } from "react-router-dom";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
} from 'reactstrap';

function NavBar(props) {

  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
  const { navItems } = props;

  const itemList = (position) => {
    return navItems.filter((item) => position ? position === item.position : true).map((item) => {
      const linkItem = (item) => {
        return (
          <NavItem key={item.url}>
            <RouterLink exact={item.exact}
              to={item.url}
              className="nav-link">
              {item.text}
            </RouterLink>
          </NavItem>
        )
      }
      const componentItem = (item) => {
        return (
          <NavItem className={item.className}>
            {item.component}
          </NavItem>
        )
      }
      return (item.component ? componentItem(item) : linkItem(item))
    })
  }

  return (
    <Navbar className="fixed-top" color="light" light expand="md">
      <NavbarBrand href="/">Prenotazione Vaccini Covid</NavbarBrand>
      <NavbarToggler onClick={toggle} />
      <Collapse isOpen={isOpen} navbar>
        <Nav className="mr-auto" navbar>
          {itemList("left")}
        </Nav>
        <Nav className="ml-auto" navbar>
          {itemList("right")}
        </Nav>
      </Collapse>


    </Navbar>
  );
}

export default NavBar;
