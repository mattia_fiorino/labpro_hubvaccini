import React from 'react';
import DayPicker from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import {DropdownItem} from "reactstrap";

export default class Giorni extends React.Component {
  constructor(props) {
    super(props);
    this.handleDayClick = this.handleDayClick.bind(this);
  }

  handleDayClick(day, modifiers = {}) {
    if (!modifiers.highlighted) {
      return;
    }
    this.props.setDataSelezionata(modifiers.selected ? undefined : day)
  }

  render() {
    const dateList = this.props.dateList
    const highlitedList = dateList.map((date) => {
        return (
            new Date(date.data)
        )
    })
    const birthdayStyle = `.DayPicker-Day--highlighted {
      background-color: orange;
      color: white;
    }`;

    const modifiers = {
      highlighted: highlitedList,
    };

    return (
      <div>
        <style>{birthdayStyle}</style>
        <DayPicker
          selectedDays={this.props.dataSelezionata}
          modifiers={modifiers}
          onDayClick={this.handleDayClick}
        />
      </div>
    );
  }
}