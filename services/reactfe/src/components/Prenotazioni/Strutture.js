import React, { useState }  from 'react';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

function Strutture(props){
    const [dropdownOpen, setDropdownOpen] = useState(false);
    const [dropdownName, setdropdownName] = useState("seleziona struttura");
    const toggle = () => setDropdownOpen(prevState => !prevState);

    const {strutture} = props;
    const {setStrutturaSelezionata} = props;

    function click (strut){
        setdropdownName(strut.ragioneSociale)
        setStrutturaSelezionata(strut)
    }

    const struttureList = strutture.map((strut) => {
        return (
            <DropdownItem onClick={() => click(strut)}>{strut.ragioneSociale}</DropdownItem>
        )
    })
    return (
        <Dropdown isOpen={dropdownOpen} toggle={toggle}>
            <DropdownToggle caret>
                {dropdownName}
            </DropdownToggle>
            <DropdownMenu>
                {struttureList}
            </DropdownMenu>
        </Dropdown>
    );
}

export default Strutture;