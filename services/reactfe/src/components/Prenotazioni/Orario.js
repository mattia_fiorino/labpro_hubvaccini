import React, { useState }  from 'react';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

function Orario(props) {
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const toggle = () => setDropdownOpen(prevState => !prevState);

  const { orari } = props;
  const { setSlotSelezionato } = props;
  const { dropdownName } = props;
  const { setDropdownName } = props;

  function click (ora){
      const data = new Date(ora.data)
      var minuti = (data.getMinutes()).toString()
      minuti = (minuti.length == 1 ? "0"+minuti : minuti)
      setDropdownName(data.getHours()+":"+minuti)
      setSlotSelezionato(ora)
  }
  const orariList = orari.map((ora) => {
      const data = new Date(ora.data)
      var minuti = (data.getMinutes()).toString()
      minuti = (minuti.length == 1 ? "0"+minuti : minuti)
      return (
          <DropdownItem onClick={() => click(ora)}>{data.getHours()}:{minuti}</DropdownItem>
      )
  })

  return (
      <Dropdown isOpen={dropdownOpen} toggle={toggle}>
          <DropdownToggle caret>
              {dropdownName}
          </DropdownToggle>
          <DropdownMenu>
              {orariList}
          </DropdownMenu>
      </Dropdown>
  );
}

export default Orario;