import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import DayPicker from 'react-day-picker';

const ModalExample = (props) => {
  const {
    buttonLabel,
    className,
    disabled,
    struttura,
    giorno,
    slot
  } = props;

  const [modal, setModal] = useState(false);
  const [modalSuccess, setModalSuccess] = useState(false);
  const [modalFail, setModalFail] = useState(false);

  const toggle = () => setModal(!modal);
  const toggleResult = () => setModalFail(!modalFail);
  const toggleRefresh = () =>  window.location.reload(true);

  const prenota = async () => {
    await fetch(`http://${process.env.REACT_APP_UTENTESERVICE_HOST}/api/Appuntamenti/ConfirmPrenotazione/${encodeURIComponent(slot._id)}`, {method: 'POST', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify({ userid: '5ea01535-dfd1-4aa6-8f5b-184cc9dfc129'})})
        .then(async response => {
            const data = await response.json();

            // check for error response
            if (!response.ok) {
                // get error message from body or default to response statusText
                const error = (data && data.message) || response.statusText;
                return Promise.reject(error);
            }
            if(!data.Errore) {
                setModal(false)
                setModalSuccess(true)
            } else {
                setModal(false)
                setModalFail(true)
            }
        }).catch(error => {
            this.setState({ errorMessage: error.toString() });
            console.error('There was an error!', error);
            setModal(false)
            setModalFail(true)
        });

  }
  const data = new Date(slot.data)
  var minuti = (data.getMinutes()).toString()
  minuti = (minuti.length == 1 ? "0"+minuti : minuti)
  const testo = disabled ? "" : "Stai prenotando la seguente vaccinazione : "+struttura.ragioneSociale+" "+giorno.toLocaleDateString()+" "+data.getHours()+":"+minuti+". Per confermare premi PRENOTA.";
  return (
    <div>
      <Button color="success" onClick={toggle} disabled={disabled}>{buttonLabel}</Button>
      <Modal isOpen={modal} toggle={toggle} className={className}>
        <ModalHeader toggle={toggle}>Riassunto prenotazione</ModalHeader>
        <ModalBody>
          {testo}
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={prenota}>PRENOTA</Button>{' '}
          <Button color="secondary" onClick={toggle}>ANNULLA</Button>
        </ModalFooter>
      </Modal>
      <Modal isOpen={modalFail} toggle={toggle} className={className}>
        <ModalHeader toggle={toggle}>Fallimento</ModalHeader>
        <ModalBody>
          A causa di un problema non è stato possibile portare a termine l'operazione, ci scusiamo per il disagio
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={toggleResult}>CHIUDI</Button>
        </ModalFooter>
      </Modal>
      <Modal isOpen={modalSuccess} toggle={toggle} className={className}>
        <ModalHeader toggle={toggle}>Fallimento</ModalHeader>
        <ModalBody>
          Prenotazie avvenuta con successo!
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={toggleRefresh}>CHIUDI</Button>
        </ModalFooter>
      </Modal>
    </div>
  );
}

export default ModalExample;