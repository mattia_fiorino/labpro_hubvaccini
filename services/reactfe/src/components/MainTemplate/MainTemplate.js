import imm1 from '../../img/login.jpg';
import imm2 from '../../img/registrazione.jpg';
import imm3 from '../../img/appuntamento.jpg';
function Corpo() {
  return (
    <section class="projects-section bg-light" id="about">
            <div class="container">

                <div class="row justify-content-center no-gutters mb-5 mb-lg-0">
                    <div class="col-lg-6"><img class="img-fluid" src={imm2} alt="" /></div>
                    <div class="col-lg-6">
                        <div class="bg-black text-center h-100 project">
                            <div class="d-flex h-100">
                                <div class="project-text w-100 my-auto text-center text-lg-left">
                                    <h4 class="text-white">1. Registrati</h4>
                                    <p class="mb-0 text-white-50">Clicca dal menù in alto e segui le istruzioni.</p>
                                    <hr class="d-none d-lg-block mb-0 ml-0" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row justify-content-center no-gutters">
                    <div class="col-lg-6"><img class="img-fluid" src={imm1} alt="" /></div>
                    <div class="col-lg-6 order-lg-first">
                        <div class="bg-black text-center h-100 project">
                            <div class="d-flex h-100">
                                <div class="project-text w-100 my-auto text-center text-lg-right">
                                    <h4 class="text-white">2. Login</h4>
                                    <p class="mb-0 text-white-50">Clicca dal menù in alto e segui le istruzioni.</p>
                                    <hr class="d-none d-lg-block mb-0 mr-0" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-center no-gutters mb-5 mb-lg-0">
                    <div class="col-lg-6"><img class="img-fluid" src={imm3} alt="" /></div>
                    <div class="col-lg-6">
                        <div class="bg-black text-center h-100 project">
                            <div class="d-flex h-100">
                                <div class="project-text w-100 my-auto text-center text-lg-left">
                                    <h4 class="text-white">3. Prendi l'appuntamento</h4>
                                    <p class="mb-0 text-white-50">Una volta fatto il login verrai portato alla tua dashboard in cui potrai controllare l'appuntamento già preso o prenderne uno nuovo. </p>
                                    <hr class="d-none d-lg-block mb-0 ml-0" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
  );
}

export default Corpo;
