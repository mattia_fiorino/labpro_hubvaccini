import React, { useState, useEffect, } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input, Spinner } from 'reactstrap';
import LoginAlert from './LoginAlert'
import { login, useAuth, } from './auth'

const alertInitialState = {
    alertMessage: '',
    visible: false,
    color: "success",
    withToggle: false
}

const ModalLogin = (props) => {
    const { buttonLabel } = props;

    const [isOpen, setModal] = useState(false);
    const toggle = () => setModal(!isOpen);

    const [email, setEmail] = useState(undefined);
    const [validEmail, setValidEmail] = useState('');
    const [password, setPassword] = useState(undefined);
    const [error, setError] = useState({
        email: "",
        password: ""
    });

    const [alert, setAlert] = useState(alertInitialState)
    const [showSpinning, setShowSpinning] = useState(false);
    const [loggedIn] = useAuth();

    const close = () => {
        setEmail(undefined);
        setValidEmail('');
        setPassword(undefined);
        setError({
            email: "",
            password: "",
        })
        setAlert(alertInitialState);
        setModal(false);
    };

    useEffect(() => {
        if (loggedIn) {
            returnedComponent = null;
        } else {
            returnedComponent = component;
        }
        console.log(loggedIn);
    }, [loggedIn])

    const validateEmail = (e) => {
        const emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (emailRex.test(e.target.value)) {
            setEmail(e.target.value);
            setValidEmail('has-success');
        } else {
            setEmail(undefined);
            setValidEmail('has-danger');
        }
    };

    const checkInputFields = () => {
        // Check email field
        if (validEmail === "has-danger") {
            setError({
                email: "Email non valida.",
                password: error.password,
            })
            return false;
        } else if (email === undefined || email === "") {
            setError({
                email: "Questo campo è richiesto.",
                password: error.password,
            })
            return false;
        } else {
            setError({
                email: "",
                password: error.password,
            })
        }

        if (password === undefined || password === "") {
            setError({
                email: error.email,
                password: "Questo campo è richiesto.",
            })
            return false;
        } else {
            setError({
                email: error.email,
                password: "",
            })
        }

        setError({
            email: "",
            password: "",
        })
        return true;
    };

    const handleLoginError = (response) => {
        if (!response.ok) {
            throw Error(response.status);
        }
        return response.json();
    }

    var onLoginClick = async () => {
        setAlert(alertInitialState);

        if (checkInputFields()) {
            setShowSpinning(true);
            const requestOpt = {
                method: "POST",
                headers: {
                    'Authorization': `Basic ${btoa(`${email}:${password}`)}`
                }
            }

            await fetch(
                `http://${process.env.REACT_APP_UTENTESERVICE_HOST}/login`,
                requestOpt).then(
                    handleLoginError
                ).then(
                    json => {
                        login(json);
                        setAlert({
                            alertMessage: 'Login effettuato!',
                            visible: true,
                            color: "success",
                            withToggle: false
                        });
                        setTimeout(() => close(), 500);
                    }).catch(
                        err => {
                            if (err.message === '401') {
                                setAlert({
                                    alertMessage: 'Email o password errate.',
                                    visible: true,
                                    color: "danger",
                                    withToggle: true
                                });
                            } else {
                                setAlert({
                                    alertMessage: 'Servizio di login attualmente non disponibile. Riprovare più tardi.',
                                    visible: true,
                                    color: "danger",
                                    withToggle: true
                                });
                            }
                        }
                    );

            setShowSpinning(false);
        }
    }

    var component = (
        <div>
            <Button color="primary" size="sm" onClick={toggle}>{buttonLabel}</Button>
            <Modal isOpen={isOpen} toggle={close} className="loginModal">
                <ModalHeader toggle={close}>Login</ModalHeader>
                <ModalBody>
                    <LoginAlert data={alert} setData={setAlert} />
                    <Form>
                        <FormGroup>
                            <Label for="emailField">Email</Label>
                            <Input
                                type="email"
                                name="email"
                                id="emailField"
                                placeholder="Email"
                                valid={validEmail === 'has-success'}
                                invalid={validEmail === 'has-danger'}
                                onChange={validateEmail} />
                            <span style={{ color: "red" }}>{error.email}</span>
                        </FormGroup>
                        <FormGroup>
                            <Label for="passwordField">Password</Label>
                            <Input
                                type="password"
                                name="password"
                                id="passwordField"
                                placeholder="Password"
                                onChange={(e) => setPassword(e.target.value)} />
                        </FormGroup>
                    </Form>
                    <a color="link" href="/richiesta_recupero_password">Ho dimenticato la password</a>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={onLoginClick}>Login {showSpinning ? <Spinner size="sm" color="light"></Spinner> : null}</Button>{' '}
                    <Button color="danger" onClick={close}>Annulla</Button>
                </ModalFooter>
            </Modal>
        </div>
    )

    var returnedComponent = loggedIn ? null : component

    return (returnedComponent);
}

export default ModalLogin;