import React from 'react';
import { Alert } from 'reactstrap';

const LoginAlert = (props) => {

    const { data: {alertMessage, visible, color, withToggle}, setData } = props;

    const onDismiss = () => setData({
        alertMessage: alertMessage, 
        visible: false, 
        color: color, 
        withToggle: withToggle
    });

    const alertReturned = () => {
        if (withToggle) {
            return (<Alert color={color} isOpen={visible} toggle={onDismiss}>{alertMessage}</Alert>)
        } else {
            return (<Alert color={color} isOpen={visible}>{alertMessage}</Alert>)
        }
    }

    return alertReturned();
}

export default LoginAlert;