import { createAuthProvider } from 'react-token-auth';


const [useAuth, authFetch, login, logout] =
    createAuthProvider({
        accessTokenKey: 'token'
    });

const logoutMethod = () => {
    authFetch('http://localhost:5051/login', {method: "DELETE",});
    logout();
}

export { useAuth, authFetch, login, logoutMethod, logout }