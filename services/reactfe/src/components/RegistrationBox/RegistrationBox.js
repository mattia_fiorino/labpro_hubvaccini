import React, { useState } from "react";
import {
    Card, Button, CardHeader, CardBody,
    CardTitle, CardText, Form, Row, Col, FormGroup, Input, Label
} from 'reactstrap';
import LoginAlert from "../Auth/LoginAlert";

const alertInitialState = {
    alertMessage: '',
    visible: false,
    color: "success",
    withToggle: false
}


function RegistrationBox(props) {

    const [alert, setAlert] = useState(alertInitialState)

    const [succesMessage, setSuccessMessage] = useState(true)

    const handleSubmit = (event) => {
        event.preventDefault();

        let nome = event.target[0].value;
        let cognome = event.target[1].value;
        let codicefiscale = event.target[2].value;
        let telefono = event.target[3].value;
        let email = event.target[4].value;
        let password = event.target[5].value;
        let confermapassword = event.target[6].value;

        if (!validate(nome, cognome, codicefiscale, telefono, email, password, confermapassword)) {
            return false;
        }

        fetch(`http://${process.env.REACT_APP_UTENTESERVICE_HOST}/register`, {
            method: 'post',
            body: `{"nome": "${nome}",
            "cognome": "${cognome}",
            "cf": "${codicefiscale}",
            "telefono": "${telefono}",
            "email": "${email}",
            "password": "${password}",
            "confirm_password": "${confermapassword}"}`
        })
            .then(response => response.text().then(function (text) {
                if (text.includes("Utente registrato")) {
                    setSuccessMessage(false)
                } else {
                    alert(text)
                }
            }))
            .then(data => console.log(data));

    }


    const validate = (nome, cognome, codicefiscale, telefono, email, password, confermapassword) => {

        let isValid = true;

        if (!nome) {

            setAlert({
                alertMessage: "Devi inserire un nome",
                visible: true,
                color: "danger",
                withToggle: false
            });
            return false;
        }

        if (!cognome) {
            setAlert({
                alertMessage: "Devi inserire un cognome",
                visible: true,
                color: "danger",
                withToggle: false
            });


            return false;
        }

        if (!codicefiscale) {
            setAlert({
                alertMessage: "Devi inserire un Codice Fiscale",
                visible: true,
                color: "danger",
                withToggle: false
            });
            return false;
        }

        if (!telefono) {
            setAlert({
                alertMessage: "Devi inserire un numero di telefono",
                visible: true,
                color: "danger",
                withToggle: false
            });
            return false;
        }

        if (!email) {
            setAlert({
                alertMessage: "Devi inserire un email",
                visible: true,
                color: "danger",
                withToggle: false
            });
            return false;
        }

        if (!password) {
            setAlert({
                alertMessage: "Devi inserire una password",
                visible: true,
                color: "danger",
                withToggle: false
            });
            return false;
        }

        if (!confermapassword) {
            setAlert({
                alertMessage: "Devi inserire una password",
                visible: true,
                color: "danger",
                withToggle: false
            });
            return false;
        }

        if (password !== confermapassword) {
            setAlert({
                alertMessage: "Le due password inserite non coincidono",
                visible: true,
                color: "danger",
                withToggle: false
            });
            return false;
        }

        if (/.+@.+\.[A-Za-z]+$/.test(email) === false) {
            setAlert({
                alertMessage: "Mail non valida",
                visible: true,
                color: "danger",
                withToggle: false
            });
            return false;
        }


        return true;
    }

    return (
        <Card>
            <CardHeader>Registrati</CardHeader>
            {succesMessage ?
                <CardBody>

                    <CardText>Per registrarti completa con cura i seguenti campi.</CardText>
                    <LoginAlert data={alert} setData={setAlert} />
                    <Form onSubmit={handleSubmit}>
                        <Row form>
                            <Col md={4}>
                                <FormGroup row>
                                    <Label for="nome" sm={4}>Nome</Label>
                                    <Col sm={8}><Input type="text" name="nome" id="nome" placeholder="Nome" /></Col>
                                    <Label for="cognome" sm={4}>Cognome</Label>
                                    <Col sm={8}><Input type="text" name="cognome" id="cognome" placeholder="Cognome" /></Col>
                                    <Label for="codicefiscale" sm={4}>Codice Fiscale</Label>
                                    <Col sm={8}><Input type="text" name="codicefiscale" id="codicefiscale" placeholder="Codice Fiscale" /></Col>
                                </FormGroup>


                                <FormGroup row>
                                    <Label for="telefono" sm={4}>Telefono</Label>
                                    <Col sm={8}><Input type="text" name="telefono" id="telefono" placeholder="Numero di telefono" /></Col>
                                    <Label for="email" sm={4}>Email</Label>
                                    <Col sm={8}><Input type="text" name="email" id="email" placeholder="mail@example.com" /></Col>


                                </FormGroup>


                                <FormGroup row>
                                    <Label for="password" sm={4}>Password</Label>
                                    <Col sm={8}><Input type="password" name="password" id="password" placeholder="Password" /></Col>
                                    <Label for="password_confirm" sm={4}>Ripeti password</Label>
                                    <Col sm={8}><Input type="password" name="password_confirm" id="password_confirm"
                                        placeholder="Password" /></Col>
                                </FormGroup>

                            </Col>


                        </Row>
                        <Button>Invia</Button>
                    </Form>

                </CardBody> :
                <CardBody><h1 className="text-success">Registrazione avvenuta con successo</h1>
                    <h3 >Ora puoi fare il login</h3></CardBody>
            }
        </Card>

    );
}

export default RegistrationBox;
