import RegistrationBox from "../../components/RegistrationBox/RegistrationBox";

function Registrazione() {
    return (
        <section class="projects-section bg-light" id="registrazione">
            <div class="container">
                <RegistrationBox/>
            </div>
        </section>
    );
}

export default Registrazione;
