import React, { useState, useEffect, }from 'react';
import { Button, Form, FormGroup, Label, Input, Spinner } from 'reactstrap';
import { Formik } from 'formik';

const RichiestaRecupero = (props) => {

    
    //const url_richiesta = window.location.href.split("3000")[0]
    const [showSpinning, setShowSpinning] = useState(false);


    return (
        <div class="container" style={{marginTop: "25px", marginBottom:"25px"}}>
            <h2>Inserisci la tua email</h2>
            <Formik
                initialValues={{ email: '' }}
                validate={values => {
                    const errors = {};
                    if (!values.email) {
                        errors.email = 'Required';
                    } else if (
                        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                    ) {
                        errors.email = 'Invalid email address';
                    }
                    return errors;
                }}
                onSubmit={(values, { setSubmitting }) => {
                    setShowSpinning(true);
                    fetch(`http://${process.env.REACT_APP_UTENTESERVICE_HOST}/reset_request?email=${values.email}`, {
                        method: "POST"
                    })
                        .then((response) => response.json())
                        .then((data) => {
                            alert(data)
                            setShowSpinning(false)
                        })
                        .catch((error) => {
                            alert(error)
                            setShowSpinning(false)
                        });
                    setSubmitting(true);
                    
                }}
            >
                {({
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting,
                    /* and other goodies */
                }) => (
                    <Form onSubmit={handleSubmit}>
                        <FormGroup>
                            <Label for="email">Email</Label>
                            <Input
                                type="email"
                                name="email"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.email}
                            />
                            {errors.email && touched.email && errors.email}
                        </FormGroup>
                        <Button type="submit" disabled={isSubmitting}>Invia richiesta{showSpinning ? <Spinner size="sm" color="light"></Spinner> : null}</Button>

                    </Form>
                )}
            </Formik>
        </div>

    );
}

export default RichiestaRecupero;