import React, { useState, useEffect, }from 'react';
import { Button, Form, FormGroup, Label, Input, Spinner } from 'reactstrap';
import { Formik } from 'formik';

const Recupero = (props) => {

    // const url_richiesta = window.location.href.split("?reset_token=")[0]
    const token = window.location.href.split("?reset_token=")[1]
    var password = ""
    const [showSpinning, setShowSpinning] = useState(false);

    return (
        <div class="container" style={{marginTop: "25px", marginBottom:"25px"}}>
            <h2>Inserisci la nuova password</h2>
            <Formik
                initialValues={{ password: "", password_re: "" }}
                validate={values => {
                    const errors = {};
                    if (!values.password) {
                        errors.password = 'Devi compilare entrambi i campi';
                    }
                    else if (values.password != values.password_re) {
                        errors.password = 'Le due password devono essere uguali'
                    }
                    return errors;
                }}
                onSubmit={(values, { setSubmitting }) => {
                    setShowSpinning(true);
                    password = values.password
                    fetch(`http://${process.env.REACT_APP_UTENTESERVICE_HOST}/reset?reset_token=${token}&password=${password}`, {
                        method: "POST"
                    })
                        .then((response) => response.json())
                        .then((data) => {
                            alert(data)
                            setShowSpinning(false)
                        })
                        .catch((error) => {
                            alert(error)
                            setShowSpinning(false)
                        });
                    setSubmitting(true);
                }}
            >
                {({
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting,
                    /* and other goodies */
                }) => (
                    <Form onSubmit={handleSubmit}>
                        <FormGroup>
                            <Label for="password">Password</Label>
                            <Input
                                type="password"
                                name="password"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.password}
                            />
                            <Label for="password_re">Ripeti la password</Label>
                            <Input
                                type="password"
                                name="password_re"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.password_re}
                            />
                            {errors.password && touched.password && errors.password}
                        </FormGroup>
                        <Button type="submit" disabled={isSubmitting}>Inserisci{showSpinning ? <Spinner size="sm" color="light"></Spinner> : null}</Button>
                    </Form>
                )}
            </Formik>
        </div>
    );
}
export default Recupero;
