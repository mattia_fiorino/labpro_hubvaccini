import React, {useState,useEffect} from "react";
import {
    Button, Card, CardBody, CardHeader, Form, FormGroup
} from 'reactstrap';
import Strutture from "../../components/Prenotazioni/Strutture";
import Giorni from "../../components/Prenotazioni/Giorni";
import Orario from "../../components/Prenotazioni/Orario";
import PrenotazioneModal from "../../components/Prenotazioni/PrenotazioneModal";

function Prenotazioni(props) {
    const handleError = (response) => {
        if (!response.ok) {
            throw Error(response.status);
        }
        return response.json();
    }
    const [disabled, setDisabled] = useState(true);
    const [struttureList, setStruttureList] = useState([]);
    const [strutturaSelezionata, setStrutturaSelezionata] = useState("");

    useEffect(() =>
        {
           fetch(`http://${process.env.REACT_APP_UTENTESERVICE_HOST}/api/Appuntamenti/ListStrutture`, { method: 'get' })
            .then(async response => {
                const data = await response.json();

                // check for error response
                if (!response.ok) {
                    // get error message from body or default to response statusText
                    const error = (data && data.message) || response.statusText;
                    return Promise.reject(error);
                }

                setStruttureList(data.ListStruttureResult)
            }).catch(error => {
                console.error('There was an error!', error);
                setStruttureList([])
            });
        },[])

    const [dateList, setDateList] = useState([]);
    const [dataSelezionata, setDataSelezionata] = useState(undefined);
    useEffect(() =>
        {
            setDateList([])
            setDataSelezionata("")
            setOrariList([])
            setSlotSelezionato("")
            setOrariDropdownName("seleziona orario appuntamento")
            if(strutturaSelezionata) {
                fetch(`http://${process.env.REACT_APP_UTENTESERVICE_HOST}/api/Appuntamenti/ListDate/${encodeURIComponent(strutturaSelezionata._id)}`, { method: 'get' })
                    .then(async response => {
                        const data = await response.json();

                        // check for error response
                        if (!response.ok) {
                            // get error message from body or default to response statusText
                            const error = (data && data.message) || response.statusText;
                            return Promise.reject(error);
                        }

                        setDateList(data.ListDateResult[strutturaSelezionata._id])
                    }).catch(error => {
                        console.error('There was an error!', error);
                        setDateList([])
                    });
            }
        },[strutturaSelezionata])

    const [orariList, setOrariList] = useState([]);
    const [slotSelezionato, setSlotSelezionato] = useState("");
    const [orariDropdownName, setOrariDropdownName] = useState("seleziona orario appuntamento");
    useEffect(() =>
        {
            setOrariList([])
            setSlotSelezionato("")
            setOrariDropdownName("seleziona orario appuntamento")
            if(dataSelezionata) {
                const month = (dataSelezionata.getMonth()+1).toString()
                const day = (dataSelezionata.getDate()).toString()
                const date = dataSelezionata.getFullYear()+"-"+(month.length == 1 ? "0"+month : month)+"-"+(day.length == 1 ? "0"+day : day)
                fetch(`http://${process.env.REACT_APP_UTENTESERVICE_HOST}/api/Appuntamenti/ListSlots/${encodeURIComponent(strutturaSelezionata._id)}/${encodeURIComponent(date)}`, { method: 'get' })
                    .then(async response => {
                        const data = await response.json();

                        // check for error response
                        if (!response.ok) {
                            // get error message from body or default to response statusText
                            const error = (data && data.message) || response.statusText;
                            return Promise.reject(error);
                        }
                        setOrariList(data.ListSlotsResult[strutturaSelezionata._id][date])
                    }).catch(error => {
                        console.error('There was an error!', error);
                        setOrariList([])
                    });
            }
        },[strutturaSelezionata,dataSelezionata])

    useEffect(() =>
        {
            if(slotSelezionato) {
                setDisabled(false)
            } else {
                setDisabled(true)
            }
        },[strutturaSelezionata,dataSelezionata,slotSelezionato])

    return (
        <section className="projects-section bg-light" id="registrazione">
            <div className="container">
                <Card>
                  <CardHeader>Compila i campi</CardHeader>
                  <CardBody>
                    <Form>
                        <FormGroup>
                            <Strutture
                                strutture={struttureList}
                                setStrutturaSelezionata={setStrutturaSelezionata}
                            />
                            <Giorni
                                dateList={dateList}
                                dataSelezionata={dataSelezionata}
                                setDataSelezionata={setDataSelezionata}
                            />
                            <Orario
                                orari={orariList}
                                dropdownName={orariDropdownName}
                                setDropdownName={setOrariDropdownName}
                                setSlotSelezionato={setSlotSelezionato}
                            />
                        </FormGroup>
                        <PrenotazioneModal
                            disabled={disabled}
                            struttura={strutturaSelezionata}
                            giorno={dataSelezionata}
                            slot={slotSelezionato}
                            buttonLabel="prenota"
                        />
                    </Form>
                  </CardBody>
                </Card>
            </div>
        </section>
    );
}

export default Prenotazioni;
