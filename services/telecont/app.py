import telepot
import time
from telepot.loop import MessageLoop
import requests
import datetime
import random
import os


prenotconthost = os.environ["PRENOTCONT_HOST"]
prenotconthost = prenotconthost + ":" + os.environ["PRENOTCONT_PORT"]


#prenotconthost = "localhost:5051"

def random_date(start, end):

    return start + datetime.timedelta(
        seconds=random.randint(0, int((end - start).total_seconds())),
    )


def handler_TELEGRAM(msg):
	content_type, chat_type, chat_id = telepot.glance(msg)

	if content_type == 'text':

		text = msg['text']
		print(chat_id, text)
		parseMessage(text, chat_id)

def parseMessage(text, chat_id):
	
	if "prenota" in text.lower():
		params = text.split()
		try:
			nome = params[1]
			cf = params[2]
		except:
			return bot.sendMessage(chat_id, "Inserire nome e codice fiscale")
		dataprenotazione = str(random_date(datetime.datetime(year=2021, month=1, day=1), datetime.datetime(year=2021, month=12, day=30)))
		msg = "Prenotazione per\n" + nome + " CF: " + cf + "\n" + "Data: " + dataprenotazione
		bot.sendMessage(chat_id, msg)
		r = requests.get('http://'+prenotconthost+'/InsertPrenotazioneUtente?nome='+nome+'&cf='+cf+'&dataprenotazione='+dataprenotazione+'&allergie=No')
		bot.sendMessage(chat_id, r.text)
	elif "controlla" in text.lower():
		params = text.split()
		try:
			cf = params[1]
		except:
			return bot.sendMessage(chat_id, "Inserire il codice fiscale")
		bot.sendMessage(chat_id, "Controllo il codice fiscale " + cf)
		r = requests.get('http://'+prenotconthost+'/CheckUtentePresente?cf='+cf)
		bot.sendMessage(chat_id, r.text)


#cloudco2020prenotationbot
#-----------------------------------------------------------------------------------------
bot = telepot.Bot("1518506384:AAEqf4u668lZdervLIichy4q32G0ZDusMXw")#cloudco2020prenotationbot
MessageLoop(bot, handler_TELEGRAM).run_as_thread()

#-----------------------------------------------------------------------------------------





while 1:
	time.sleep(100)