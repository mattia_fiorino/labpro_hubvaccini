import pytest
import dockerfile
import os
import logging
from unittest import mock
import mongomock
import datetime
import importlib.util
from uuid import uuid4
from werkzeug.security import generate_password_hash


@pytest.fixture
def utenteserviceApp():
    # Import the whole app.py without stardard python import method
    # So the test can be executed without worrying where pytest is launched
    with mock.patch.dict(os.environ, env_vars()):
        utenteservice_app_spec = importlib.util.spec_from_file_location(
            'utenteserviceApp', './services/utenteservice/app.py')
        utenteservice_app = importlib.util.module_from_spec(utenteservice_app_spec)
        utenteservice_app_spec.loader.exec_module(utenteservice_app)
        yield utenteservice_app


@pytest.fixture
def User():
    # Import the whole User.py model without stardard python import method
    User_spec = importlib.util.spec_from_file_location(
        'User', './services/utenteservice/sources/models/User.py')
    User = importlib.util.module_from_spec(User_spec)
    User_spec.loader.exec_module(User)

    # Returns the User model class
    return User.User


@pytest.fixture
def tokenBlacklist():
    # Import the whole tokenBlacklist.py model without stardard python import method
    tokenBlacklist_spec = importlib.util.spec_from_file_location(
        'tokenBlacklist', './services/utenteservice/sources/models/tokenBlacklist.py')
    tokenBlacklist = importlib.util.module_from_spec(tokenBlacklist_spec)
    tokenBlacklist_spec.loader.exec_module(tokenBlacklist)

    # Returns the tokenBlacklist model class
    return tokenBlacklist.tokenBlacklist


@pytest.fixture
def utenteserviceClient(utenteserviceApp, User):
    # Import configuration script
    config_spec = importlib.util.spec_from_file_location(
        'utenteserviceApp', './services/utenteservice/config.py')
    config = importlib.util.module_from_spec(config_spec)
    config_spec.loader.exec_module(config)

    # Create Flask app with mongomock
    app = utenteserviceApp.create_app(config.testConfigs(isMock=True))

    # Run the test_client
    test_client = app.test_client()

    # Insert a test user
    user = User(
        public_id=uuid4(),
        email='test@gmail.com',
        password=generate_password_hash('password'),
        nome='Mario',
        cognome='Rossi',
        cf='BIU7BI098I',
        telefono='089375089',
        deleted=False
    )
    try:
        user.save()
    except Exception as e:
        pass

    return test_client

# The Flask app is now started with appropriate configuration for testing
# No need for env variables moking anymore
def env_vars() -> dict:
    df_content = (x for x in dockerfile.parse_file(
         './services/utenteservice/Dockerfile') if x.cmd == 'env')
    envs = {}
    for env in df_content:
         envs[env.value[0]] = env.value[1].strip('\'')
    return envs


# Usefull now because the old utenteservices endpoint are no longer used
# def defineSomePrenotazioni() -> dict:
#     prenotazioni = {}
#     prenotazioni['prenotazioni'] = []
#     prenotazioni['prenotazioni'].append({
#         'nome': 'Francesco Colombo',
#         'cf': 'CLMFNC70M05G535O',
#         'dataprenotazione': str(datetime.date(2021, 3, 1)),
#         'allergie': 'Morfina'
#     })
#     prenotazioni['prenotazioni'].append({
#         'nome': 'Alice Ricci',
#         'cf': ' LCARCC61S46G337H',
#         'dataprenotazione': str(datetime.date(2021, 3, 11)),
#         'allergie': ''
#     })
#     prenotazioni['prenotazioni'].append({
#         'nome': 'Greta Martini',
#         'cf': 'GRTMTN88D44A662C',
#         'dataprenotazione': str(datetime.date(2021, 2, 27)),
#         'allergie': 'Cefalosporine'
#     })
#     prenotazioni['prenotazioni'].append({
#         'nome': 'Riccardo Villa',
#         'cf': 'RCCVLL64C08D205L',
#         'dataprenotazione': str(datetime.date(2021, 3, 16)),
#         'allergie': ''
#     })
#     return prenotazioni
