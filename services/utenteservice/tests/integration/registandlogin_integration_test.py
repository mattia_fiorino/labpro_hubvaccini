import base64

def login(app, email, password):
    base64_credential = base64.b64encode(
        f"{email}:{password}".encode('utf-8')).decode('utf-8')
    response = app.post(
        '/login',
        headers={'Authorization': f"Basic {base64_credential}"}
    )
    return response


def test_loginController_postLogin(utenteserviceClient):
    response = login(utenteserviceClient, 'test@gmail.com', 'password')
    assert response.json['token']


def register(app, nome, cognome, cf, telefono, email, password, confirm_password):
    user_data = {"nome": nome,
        "cognome": cognome,
        "cf": cf,
        "telefono": telefono,
        "email": email,
        "password": password,
        "confirm_password": confirm_password}

    response = app.post(
        '/register',
        json = user_data

    )
    return response

def test_registerController_postRegistration(utenteserviceClient):
    register(utenteserviceClient, 'Jimmy', 'Page', 'PGAJMM91D21F201Y', '333333333', 'jimmypage@gmail.com', 'led', 'led')
    response = login(utenteserviceClient, 'jimmypage@gmail.com', 'led')
    print(response.data)
    assert response.json['token']


def test2_registerController_postRegistration(utenteserviceClient):
    register(utenteserviceClient, 'Robert', 'Page', 'PGAJMM91D21F201Y', '333333333', 'robpage@gmail.com', 'zep', 'zep')
    response = login(utenteserviceClient, 'robpage@gmail.com', 'zop')
    print(response.data)
    assert "Could not verify" in str(response.data)

'''
data = '''