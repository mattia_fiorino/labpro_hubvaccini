def register(app, nome, cognome, cf, telefono, email, password, confirm_password):
    user_data = {"nome": nome,
        "cognome": cognome,
        "cf": cf,
        "telefono": telefono,
        "email": email,
        "password": password,
        "confirm_password": confirm_password}

    response = app.post(
        '/register',
        json = user_data

    )
    return response

def test_registerController_postRegistration(utenteserviceClient):
    response = register(utenteserviceClient, 'Enrico', 'Lucia', 'LCUFDR91D21F201Y', '333333333', 'zedef@gmail.com', 'pippo88!!', 'pippo88!!')
    print(response.data)
    assert "Utente" in str(response.data) #This checks that the user has been correctly created, the message says "Utente registrato .."

def test_registerController_postDoubleRegistration(utenteserviceClient):
    response = register(utenteserviceClient, 'Enrico', 'Lucia', 'LCUFDR91D21F201Y', '333333333', 'zedef@gmail.com', 'pippo88!!', 'pippo88!!')
    print(response.data)
    assert "Questa mail" in str(response.data) #This checks that we receive an error stating that there is already a user with that email



'''
data = '''