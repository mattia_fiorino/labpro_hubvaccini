from pytest import fixture
from uuid import uuid4
import importlib


@fixture
def user(User):

    return User(
        public_id=uuid4(),
        email='test@gmail.com',
        password='password',
        nome='Mario',
        cognome='Rossi',
        cf='BIU7BI098I',
        telefono='089375089',
        deleted=False
    )


def test_User_create(user):
    assert user
