from werkzeug.security import generate_password_hash
import base64
import json

def login(app, email, password):
    base64_credential = base64.b64encode(
        f"{email}:{password}".encode('utf-8')).decode('utf-8')
    response = app.post(
        '/login',
        headers={'Authorization': f"Basic {base64_credential}"}
    )
    return response

def reset(app, reset_token, password):
    response = app.post('/reset', query_string=dict(reset_token=reset_token, password=password))
    return response

def test_reset(utenteserviceClient, tokenBlacklist):
    reset_token = login(utenteserviceClient, 'test@gmail.com', 'password').json['token']

    response = reset(utenteserviceClient, reset_token, generate_password_hash("prova"))
    assert "password cambiata con successo" in str(response.data)