import json
import uuid
from unittest.mock import patch

# from flask.testing import FlaskClient
from flask import Flask

from services.utenteservice.sources.models.User import User
#from services.utenteservice.tests.conftest import utenteserviceClient


class TestListStruttureResource:
    @patch('services.utenteservice.sources.controllers.appuntamenti_controller.requests.get')
    def test_get(self, mock_get, utenteserviceClient):  # noqa
        User(nome='Ross',
             cognome='Lawley',
             email='prova@prova.it',
             password='prova',
             cf='PRVPRV00C34O234G',
             telefono='02112233').save()
        mock_get.return_value.headers.add('Content-Type', 'application/json')
        mock_get.return_value.json.return_value = json.loads(
            '{"ListStruttureResult":[{"id":1,"nome":"struttura 1"},{"id":2,"nome":"struttura 2"}]}')
        results = utenteserviceClient.get('/api/Appuntamenti/ListStrutture',
                                          headers=dict(Authorization='Bearer PRVPRV00C34O234G'),
                                          follow_redirects=True).get_json()

        assert results == json.loads(
            '{"ListStruttureResult":[{"id":1,"nome":"struttura 1"},{"id":2,"nome":"struttura 2"}]}')

        mock_get.return_value.ok = False
        mock_get.return_value.status_code = 500
        results = utenteserviceClient.get('/api/Appuntamenti/ListStrutture',
                                          headers=dict(Authorization='Bearer PRVPRV00C34O234G'),
                                          follow_redirects=True).get_json()

        assert results == json.loads('{"Errore":"Problema di comunicazione con il servizio delle strutture"}')


class TestListDateResource:
    @patch('services.utenteservice.sources.controllers.appuntamenti_controller.requests.get')
    def test_get(self, mock_get, utenteserviceClient):  # noqa
        mock_get.return_value.headers.add('Content-Type', 'application/json')
        mock_get.return_value.json.return_value = json.loads(
            '{"ListDateResult":[{"id":1,"data":"2021-04-01","slots_disp":24},{"id":2,"data":"2021-04-02","slots_disp":120},{"id":3,"data":"2021-04-03","slots_disp":200}]}')
        results = utenteserviceClient.get('/api/Appuntamenti/ListDate/1',
                                          headers=dict(Authorization='Bearer PRVPRV00C34O234G'),
                                          follow_redirects=True).get_json()

        assert results == json.loads(
            '{"ListDateResult":{"1":[{"id":1,"data":"2021-04-01","slots_disp":24},{"id":2,"data":"2021-04-02","slots_disp":120},{"id":3,"data":"2021-04-03","slots_disp":200}]}}')

        mock_get.return_value.ok = False
        mock_get.return_value.status_code = 404
        results = utenteserviceClient.get('/api/Appuntamenti/ListDate/1',
                                          headers=dict(Authorization='Bearer PRVPRV00C34O234G'),
                                          follow_redirects=True).get_json()

        assert results == json.loads('{"Errore":"Nessuna data disponibile per la struttura selezionata"}')

        mock_get.return_value.ok = False
        mock_get.return_value.status_code = 500
        results = utenteserviceClient.get('/api/Appuntamenti/ListDate/1',
                                          headers=dict(Authorization='Bearer PRVPRV00C34O234G'),
                                          follow_redirects=True).get_json()

        assert results == json.loads('{"Errore":"Problema di comunicazione con il servizio delle strutture"}')


class TestListSlotResource:
    @patch('services.utenteservice.sources.controllers.appuntamenti_controller.requests.get')
    def test_get(self, mock_get, utenteserviceClient):  # noqa
        mock_get.return_value.headers.add('Content-Type', 'application/json')
        mock_get.return_value.json.return_value = json.loads(
            '{"ListDateResult":[{"id":1,"data":"2021-04-01","ora":"12:30"},{"id":2,"data":"2021-04-01","ora":"12:45"},{"id":3,"data":"2021-04-01","ora":"13:00"}]}')
        results = utenteserviceClient.get('/api/Appuntamenti/ListSlots/1/2021-04-01',
                                          headers=dict(Authorization='Bearer PRVPRV00C34O234G'),
                                          follow_redirects=True).get_json()

        assert results == json.loads(
            '{"ListSlotsResult":{"1":{"2021-04-01":[{"id":1,"data":"2021-04-01","ora":"12:30"},{"id":2,"data":"2021-04-01","ora":"12:45"},{"id":3,"data":"2021-04-01","ora":"13:00"}]}}}')

        mock_get.return_value.ok = False
        mock_get.return_value.status_code = 404
        results = utenteserviceClient.get('/api/Appuntamenti/ListSlots/1/20210401',
                                          headers=dict(Authorization='Bearer PRVPRV00C34O234G'),
                                          follow_redirects=True).get_json()

        assert results == json.loads('{"Errore":"Nessuno slot trovato per la struttura e la data selezionate"}')

        mock_get.return_value.ok = False
        mock_get.return_value.status_code = 500
        results = utenteserviceClient.get('/api/Appuntamenti/ListSlots/1/20210401',
                                          headers=dict(Authorization='Bearer PRVPRV00C34O234G'),
                                          follow_redirects=True).get_json()

        assert results == json.loads('{"Errore":"Problema di comunicazione con il servizio delle strutture"}')


class TestConfirmPrenotazioneResource:
    @patch('services.utenteservice.sources.controllers.appuntamenti_controller.requests.post')
    def test_get(self, mock_post, utenteserviceClient):  # noqa
        mock_post.return_value.headers.add('Content-Type', 'application/json')
        mock_post.return_value.json.return_value = json.loads(
            '{"id":1,"data":"2021-04-01","ora":"12:30","prenotato":1,"id_utente":"1234"}')
        results = utenteserviceClient.post('/api/Appuntamenti/ConfirmPrenotazione/1',
                                           headers=dict(Authorization='Bearer PRVPRV00C34O234G'),
                                           json={"userid": "123"},
                                           follow_redirects=True).get_json()

        assert results == json.loads(
            '{"ConfirmPrenotazioneResult":{"id":1,"data":"2021-04-01","ora":"12:30","prenotato":1,"id_utente":"1234"}}')

        mock_post.return_value.ok = False
        mock_post.return_value.status_code = 404
        results = utenteserviceClient.post('/api/Appuntamenti/ConfirmPrenotazione/1',
                                           headers=dict(Authorization='Bearer PRVPRV00C34O234G'),
                                           json={"userid": "123"},
                                           follow_redirects=True).get_json()

        assert results == json.loads('{"Errore":"Nessuno slot trovato corrispondente all\'id selezionato"}')

        mock_post.return_value.ok = False
        mock_post.return_value.status_code = 204
        results = utenteserviceClient.post('/api/Appuntamenti/ConfirmPrenotazione/1',
                                           headers=dict(Authorization='Bearer PRVPRV00C34O234G'),
                                           json={"userid": "123"},
                                           follow_redirects=True).get_json()

        assert results == json.loads('{"Errore":"Lo slot selezionato risulta già occupato"}')

        mock_post.return_value.ok = False
        mock_post.return_value.status_code = 500
        results = utenteserviceClient.post('/api/Appuntamenti/ConfirmPrenotazione/1',
                                           headers=dict(Authorization='Bearer PRVPRV00C34O234G'),
                                           json={"userid": "123"},
                                           follow_redirects=True).get_json()

        assert results == json.loads('{"Errore":"Problema di comunicazione con il servizio delle strutture"}')

        results = utenteserviceClient.post('/api/Appuntamenti/ConfirmPrenotazione/1',
                                           headers=dict(Authorization='Bearer PRVPRV00C34O234G'),
                                           follow_redirects=True).get_json()

        assert results == json.loads('{"Errore":"Non è stato specificato l\'id dell\'utente"}')

        results = utenteserviceClient.post('/api/Appuntamenti/ConfirmPrenotazione/1',
                                           headers=dict(Authorization='Bearer PRVPRV00C34O234G'),
                                           json={"other": "123"},
                                           follow_redirects=True).get_json()

        assert results == json.loads('{"Errore":"Non è stato specificato l\'id dell\'utente"}')
