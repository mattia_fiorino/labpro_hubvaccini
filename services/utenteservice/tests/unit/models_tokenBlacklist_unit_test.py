from pytest import fixture
from uuid import uuid4
import importlib
import datetime


@fixture
def tokenBlacklist():
    tokenBlacklist_spec = importlib.util.spec_from_file_location(
        'tokenBlacklist', './services/utenteservice/sources/models/tokenBlacklist.py')
    tokenBlacklist = importlib.util.module_from_spec(tokenBlacklist_spec)
    tokenBlacklist_spec.loader.exec_module(tokenBlacklist)

    return tokenBlacklist.tokenBlacklist(
        token = 'wreg544qhUY6rvfi8u76', 
        logoutDate = datetime.datetime.now()
    )


def test_tokenBlacklist_create(tokenBlacklist):
    assert tokenBlacklist
