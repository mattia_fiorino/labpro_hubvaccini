import json
import os
import pytest
import datetime
import urllib
from unittest import mock
from random import randint

# All test refers to the old utenteservice service
# and are omitted until new development

# def testIndex(utenteserviceClient):
#     index_reponse = utenteserviceClient.get('/')
#     assert index_reponse.data.decode('UTF-8') == "Servizio prenotazioni - MONGODBCONT_OST = " + \
#         os.environ["MONGODBCONT_HOST"]


# def testCheckUtentePresente(utenteserviceClient):
#     # Test utente presente
#     url_present_cf = '/CheckUtentePresente?' + \
#         urllib.parse.urlencode({'cf': 'GRTMTN88D44A662C'})
#     valid_response = utenteserviceClient.get(url_present_cf)
#     assert valid_response.data.decode('UTF-8') == 'Prenotazione trovata'

#     # Test utente non presente
#     url_not_present_cf = '/CheckUtentePresente?' + \
#         urllib.parse.urlencode({'cf': '123456'})
#     invalid_response = utenteserviceClient.get(url_not_present_cf)
#     assert invalid_response.data.decode(
#         'UTF-8') == "Prenotazione non presente per questo codice fiscale"


# def testInsertPrenotazioneUtente(utenteserviceClient):
#     prenotazione = {
#         'nome': 'Mario Rossi',
#         'cf': 'RSSMRA44H07A662B',
#         'dataprenotazione': str(datetime.date(2021, 3, 16)),
#         'allergie': ''
#     }
#     response = utenteserviceClient.get('/InsertPrenotazioneUtente?' +
#                                     urllib.parse.urlencode(prenotazione))
#     assert response.data.decode('UTF-8') == 'Prenotazione inserita correttamente per ' + \
#         prenotazione['nome'] + prenotazione['cf'] + \
#         prenotazione['dataprenotazione'] + prenotazione['allergie']


# def testPrenotazioni(utenteserviceClient):
#     from conftest import defineSomePrenotazioni
#     # Estrazione utente random
#     rand_user = defineSomePrenotazioni()['prenotazioni'][randint(0, 3)]
#     response = utenteserviceClient.get('/Prenotazioni')
#     assert rand_user in json.loads(response.data.decode('UTF-8'))
