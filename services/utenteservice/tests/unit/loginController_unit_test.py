import base64


def login(app, email, password):
    base64_credential = base64.b64encode(
        f"{email}:{password}".encode('utf-8')).decode('utf-8')
    response = app.post(
        '/login',
        headers={'Authorization': f"Basic {base64_credential}"}
    )
    return response


def test_loginController_postLogin(utenteserviceClient):
    response = login(utenteserviceClient, 'test@gmail.com', 'password')
    assert response.json['token']


def test_loginController_deleteLogout(utenteserviceClient, tokenBlacklist):
    login_response = login(utenteserviceClient, 'test@gmail.com', 'password')
    jwt_token = login_response.json['token']
    logout_response = utenteserviceClient.delete(
        '/login',
        headers={'Authorization': f"Bearer {jwt_token}"}
    )
    assert logout_response.status_code == 205
    assert tokenBlacklist.objects(token=jwt_token).get()
