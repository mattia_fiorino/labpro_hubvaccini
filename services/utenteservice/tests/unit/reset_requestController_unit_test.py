import json

def reset_request(app, email):
    response = app.post('/reset_request', query_string=dict(email=email))
    return response
    

def test_reset_request(utenteserviceClient):
    response = reset_request(utenteserviceClient, 'test@gmail.com')
    assert "email inviata con successo" in str(response.data)

def test_reset_request_error(utenteserviceClient):
    response = reset_request(utenteserviceClient, 'test@gmaili.com')
    assert "User does not exist" in str(response.data)