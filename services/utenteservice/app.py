import argparse
import json
import os
from base64 import b64encode

from flask import Flask, jsonify, request
from flask_restplus import Api
from flask_cors import CORS

from config import prodConfigs, testConfigs
from routes import register_routes


def create_app(conf) -> Flask:

    # Create the app
    app = Flask(__name__)
    
    # Setup db
    db = conf.config_mongodb(app)

    # Create RESTplus API
    api = Api(app)

    # Configure the app
    conf.config_app(app)

    # Setup the routes
    register_routes(api, app)

    CORS(app, resources={r'/*': {'origins': '*'}})

    return app


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    
    parser.add_argument('-t', '--Test', action='store_true')
    
    is_debug = parser.parse_args()

    if is_debug.Test:
        app = create_app(testConfigs(False))
        app.run(host="0.0.0.0", port=5051, debug=False)
    else:
        app = create_app(prodConfigs)
        app.run(host="0.0.0.0", port=5051)
