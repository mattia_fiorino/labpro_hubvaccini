import datetime
import uuid
import hashlib
import jwt
from flask import current_app as app
from flask import jsonify, make_response, request
from flask_restplus import Namespace, Resource
from werkzeug.security import check_password_hash, generate_password_hash
import json
from ..models.User import User
from ..models.tokenBlacklist import tokenBlacklist
from . import token_required

register_controller = Namespace('register', description='Register controller')


@register_controller.route('')
class registerController(Resource):

    def post(self):

        print(request)
        auth = request.get_json(force=True)
        #auth = json.loads(auth)
        print(auth)
        try:
            if not auth or not auth['password'] or not auth["email"] or not auth["confirm_password"] or not auth["nome"] or not auth["cognome"] or not auth["cf"] or not auth["telefono"]:
                return make_response('Missing arguments for registering the user', 501)
        except:
            return make_response('Missing arguments for registering the user', 501)


        try:
            user = User.objects(email=auth['email']).get()
            return make_response('Questa mail è già registrata', 400, {'Access-Control-Allow-Origin':'*'})
        except:
            print("[-] User not found, going to create it...")

        try:
            user = User.objects(cf=auth['cf']).get()
            return make_response('Questo codice fiscale è già registrato', 400, {'Access-Control-Allow-Origin':'*'})
        except:
            print("[-] User not found, going to create it...")

        if auth["confirm_password"] != auth["password"]:
            return make_response('The confirm password is not the same as the password', 501)

        public_id = hashlib.md5(auth["email"].encode('utf-8')).hexdigest()

        new_user = User(public_id=public_id, password=generate_password_hash(auth["password"]), email=auth["email"], nome=auth["nome"], cognome=auth["cognome"], cf=auth["cf"], telefono=auth["telefono"])
        new_user.save()

        response = make_response('Utente registrato ' + public_id + auth["email"], 203, {'Access-Control-Allow-Origin':'*'})

        response.headers['Access-Control-Allow-Origin'] = '*'

        return response
