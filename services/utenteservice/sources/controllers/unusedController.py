from flask import request
from flask_restplus import Namespace, Resource


unused_controller = Namespace(
    'unused', description='Controller for old unused functions')


@unused_controller.route('/Prenotazioni')
class Prenotazioni(Resource):
    def get(self):
        cf = request.args.get('cf')
        if db.collection.count_documents({'cf': cf}, limit=1) != 0:
            return "Prenotazione trovata"
        return "Prenotazione non presente per questo codice fiscale"

    def post(self):
        nome = request.form['nome']
        cf = request.form['cf']
        dataprenotazione = request.form['dataprenotazione']
        allergie = request.form['allergie']

        entry = {
            "nome": nome,
            "cf": cf,
            "dataprenotazione": dataprenotazione,
            "allergie": allergie

        }
        db.collection.insert_one(entry).inserted_id
        return 'Prenotazione inserita correttamente per ' + nome + cf + dataprenotazione + allergie

# @unused_controller.route("/Prenotazioni", methods=['GET'])
# def Prenotazioni():
# 	array = []
# 	for a in db.collection.find():
# 		array.append({"nome": a["nome"], "cf": a["cf"], "dataprenotazione": a["dataprenotazione"], "allergie": a["allergie"]})

# 	return json.dumps(array)
