import datetime
import uuid

import jwt
from flask import current_app as app
from flask import jsonify, make_response, request, Response
from flask_restplus import Namespace, Resource
from werkzeug.security import check_password_hash, generate_password_hash

from ..models.User import User
from ..models.tokenBlacklist import tokenBlacklist
from . import token_required

login_controller = Namespace('login', description='Login controller')


@login_controller.route('')
class loginController(Resource):

    def post(self):
        auth = request.authorization

        if not auth or not auth['username'] or not auth['password']:
            return make_response('Could not verify', 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})

        try:
            user = User.objects(email=auth['username']).get()
        except:
            return make_response('Could not verify', 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})

        if check_password_hash(user.password, auth.password):
            token = jwt.encode({'public_id': str(user.public_id), 'exp': datetime.datetime.utcnow(
            ) + datetime.timedelta(minutes=30)}, str(app.config['SECRET_KEY']), algorithm="HS256")

            return jsonify({'token': token})

        return make_response('Could not verify', 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})

    @token_required
    def delete(self, *args, **kwargs):
        logged_out = tokenBlacklist(
            token=self.token, logoutDate=datetime.datetime.utcnow())
        logged_out.save()
        return {'success': True}, 205
