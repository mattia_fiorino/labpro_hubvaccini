from flask import request, jsonify, Flask, current_app as app
from flask_accepts import accepts, responds
from flask_restplus import Namespace, Resource
from flask.wrappers import Response
from pymongo import MongoClient
import os
import requests
import datetime
import jwt
from . import token_required
from ..models.User import User
from typing import List
import json

api = Namespace('Appuntamenti', description='Single namespace, single entity')  # noqa
BASE_ROUTE = 'Appuntamenti'

@api.route('/ListStrutture')
class ListStruttureResource(Resource):
    '''ListStrutture'''
    token='1234567890'
    #@token_required
    #@responds(schema=ListStruttureSchema, many=True)
    def get(self) -> Response:
        '''Ottenere la lista di tutte le strutture in cui è disponibile uno slot per vaccinarsi'''

        flaskmainconthost = app.config["FLASKMAINCONT_HOST"]
        flaskmainconthost = flaskmainconthost + ":" + app.config["FLASKMAINCONT_PORT"]

        headers = dict(Authorization='Bearer '+self.token)
        r = requests.get('http://' + flaskmainconthost + '/api/Strutture/ListAll', headers=headers)
        if r.ok:
            result = {"ListStruttureResult": []}
            strutture = r.json()
            for s in strutture['ListStruttureResult']:
                result["ListStruttureResult"].append(s)

            response = jsonify(result)
            response.headers.add('Access-Control-Allow-Origin', '*')
            return response
        else:
            response = jsonify(dict(Errore='Problema di comunicazione con il servizio delle strutture'))
            response.headers.add('Access-Control-Allow-Origin', '*')
            return response


@api.route('/ListDate/<strutId>')
@api.param('strutId', 'Struttura database ID')
class ListDateResource(Resource):
    '''ListDate'''
    token = '1234567890'
    #@token_required
    #@responds(schema=ListDateSchema)
    def get(self, strutId) -> Response:
        '''Ottenere l'elenco dei giorni di disponibilità dato un id struttura'''

        flaskmainconthost = app.config["FLASKMAINCONT_HOST"]
        flaskmainconthost = flaskmainconthost + ":" + app.config["FLASKMAINCONT_PORT"]

        headers = dict(Authorization='Bearer '+self.token)
        r = requests.get('http://' + flaskmainconthost + '/api/Slot/List/'+strutId, headers=headers)
        if r.ok:
            result = {"ListDateResult": {strutId: []}}
            date = r.json()
            print(date)
            for s in date['ListDateResult']:
                result["ListDateResult"][strutId].append(s)

            response = jsonify(result)
            response.headers.add('Access-Control-Allow-Origin', '*')
            return response
        else:
            if r.status_code == 404:
                response = jsonify(dict(Errore='Nessuna data disponibile per la struttura selezionata'))
                response.headers.add('Access-Control-Allow-Origin', '*')
                return response
            else:
                response = jsonify(dict(Errore='Problema di comunicazione con il servizio delle strutture'))
                response.headers.add('Access-Control-Allow-Origin', '*')
                return response


@api.route('/ListSlots/<strutId>/<data>')
@api.param('strutId', 'Struttura database ID')
@api.param('data', 'Data slot prenotazione')
class ListSlotsResource(Resource):
    '''ListSlots'''
    token = '1234567890'
    #@token_required
    #@responds(schema=ListSlotsSchema)
    def get(self, strutId, data) -> Response:
        '''Ottenere l'elenco degli slot disponibili dato un id struttura e una data'''

        flaskmainconthost = app.config["FLASKMAINCONT_HOST"]
        flaskmainconthost = flaskmainconthost + ":" + app.config["FLASKMAINCONT_PORT"]

        headers = dict(Authorization='Bearer '+self.token)
        r = requests.get('http://' + flaskmainconthost + '/api/Slot/List/'+strutId+'/'+data, headers=headers)
        if r.ok:
            #data = datetime.datetime.strptime(data, '%Y%m%d').strftime('%Y-%m-%d')
            result = {"ListSlotsResult": {strutId: {data :[]}}}
            slots = r.json()
            for s in slots['ListDateResult']:
                result["ListSlotsResult"][strutId][data].append(s)

            response = jsonify(result)
            response.headers.add('Access-Control-Allow-Origin', '*')
            return response
        else:
            if r.status_code == 404:
                response = jsonify(dict(Errore='Nessuno slot trovato per la struttura e la data selezionate'))
                response.headers.add('Access-Control-Allow-Origin', '*')
                return response
            else:
                response = jsonify(dict(Errore='Problema di comunicazione con il servizio delle strutture'))
                response.headers.add('Access-Control-Allow-Origin', '*')
                return response


@api.route('/ConfirmPrenotazione/<slotId>')
@api.param('slotId', 'Slot database ID')
class ConfirmPrenotazioneResource(Resource):
    '''ConfirmPrenotazione'''
    token = '1234567890'
    #@token_required
    #@responds(schema=ConfirmPrenotazioneSchema)
    def post(self, slotId) -> Response:
        '''Prenotare uno slot dato l'id'''

        flaskmainconthost = app.config["FLASKMAINCONT_HOST"]
        flaskmainconthost = flaskmainconthost + ":" + app.config["FLASKMAINCONT_PORT"]

        headers = dict(Authorization='Bearer '+self.token)
        payload = request.json

        if payload and 'userid' in payload:
            r = requests.post('http://' + flaskmainconthost + '/api/Slot/Book/'+slotId, headers=headers, json={"userid":str(payload['userid'])})
            if r.ok:
                prenotazione = r.json()
                print(prenotazione)
                result = {"ConfirmPrenotazioneResult": prenotazione}

                response = jsonify(result)
                response.headers.add('Access-Control-Allow-Origin', '*')
                return response
            else:
                if r.status_code == 404:
                    response = jsonify(dict(Errore='Nessuno slot trovato corrispondente all\'id selezionato'))
                    response.headers.add('Access-Control-Allow-Origin', '*')
                    return response
                else:
                    if r.status_code == 204:
                        response = jsonify(dict(Errore='Lo slot selezionato risulta già occupato'))
                        response.headers.add('Access-Control-Allow-Origin', '*')
                        return response
                    else:
                        response = jsonify(dict(Errore='Problema di comunicazione con il servizio delle strutture'))
                        response.headers.add('Access-Control-Allow-Origin', '*')
                        return response
        else:
            response = jsonify(dict(Errore='Non è stato specificato l\'id dell\'utente'))
            response.headers.add('Access-Control-Allow-Origin', '*')
            return response