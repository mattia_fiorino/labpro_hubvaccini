from functools import wraps

import jwt
from flask import current_app as app
from flask import jsonify, make_response, request
from mongoengine import DoesNotExist

from ..models.User import User
from ..models.tokenBlacklist import tokenBlacklist

def register_registerController(api, app):
    from .registerController import register_controller
    api.add_namespace(register_controller, path='/register')

def register_loginController(api, app):
    from .loginController import login_controller
    api.add_namespace(login_controller, path='/login')


def register_unusedController(api, app):
    from .unusedController import unused_controller
    api.add_namespace(unused_controller, path='/unused')

def register_appuntamentiController(api, app):
    from .appuntamenti_controller import api as appuntamenti_controller, BASE_ROUTE as appuntamenti_base_route
    api.add_namespace(appuntamenti_controller, path=f'/api/{appuntamenti_base_route}')

def register_resetController(api, app):
    from .resetController import reset_controller
    api.add_namespace(reset_controller, path='/reset')

def register_reset_requestController(api, app):
    from .reset_requestController import reset_request_controller
    api.add_namespace(reset_request_controller, path='/reset_request')


def token_required(f):
    @wraps(f)
    def decorated(self, *args, **kwargs):

        AUTH_ERROR_RESPONSE = make_response('Authentication error', 401, {
            'message': 'Token is invalid!'})
        token = None

        try:
            token = request.headers['Authorization'].split(" ")[1]
        except:
            return AUTH_ERROR_RESPONSE

        try:
            # Query the token on the token blacklist document
            tokenBlacklist.objects(token=token).get()
            # If is foundend then the token is black listed from loginController.delete method
            return AUTH_ERROR_RESPONSE
        except DoesNotExist:
            # A DoesNotExist exception is raised if the token is not black listed
            pass
        except Exception:
            return AUTH_ERROR_RESPONSE

        try:
            data = jwt.decode(token, str(app.config['SECRET_KEY']), algorithms="HS256")
            current_user = User.objects(public_id=data['public_id']).get()
            if current_user.deleted:
                return AUTH_ERROR_RESPONSE
        except Exception as e:
            return AUTH_ERROR_RESPONSE

        self.token = token
        return f(self, *args, **kwargs)

    return decorated
