import datetime
import uuid

import jwt
from flask import current_app as app
from flask import jsonify, make_response, request, Response
from flask_restplus import Namespace, Resource
from flask_mail import Mail, Message

from ..models.User import User

reset_request_controller = Namespace('reset_request', description='request reset password controller')



@reset_request_controller.route('')
class reset_requestController(Resource):
    
    def post(self):
        mail = Mail(app)
        
        url = "http://prod.vaxhub.ellat.it/recupero_password"
        email = request.args['email']

        try:
            user = User.objects.get(email=email)
        except:
            response = jsonify("User does not exist")
            response.headers.add('Access-Control-Allow-Origin', '*')
            return response
        
        reset_token = jwt.encode({'public_id': str(user.public_id), 'exp': datetime.datetime.utcnow(
            ) + datetime.timedelta(minutes=30)}, str(app.config['SECRET_KEY']), algorithm="HS256")
        msg = Message('richiesta recupero password', sender = 'reset-pass@labprovaccini.com', recipients = [user.email])
        msg.body = ""
        msg.html = "<p>E' stato richiesto il recupero della password, se non sei stato tu a richiederlo fai una segnalazione, in caso contrario clicca sul link qui sotto</p> <a href=" + url + "?reset_token=" + reset_token +">recupero password</a>"
        mail.send(msg)
        response = jsonify("email inviata con successo")
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response