import datetime
import uuid

import jwt
from flask import current_app as app
from flask import jsonify, make_response, request, Response
from flask_restplus import Namespace, Resource
from werkzeug.security import generate_password_hash
import json

from ..models.User import User
from ..models.tokenBlacklist import tokenBlacklist

reset_controller = Namespace('reset', description='reset password controller')


@reset_controller.route('')
class resetController(Resource):

    def post(self):
        password = request.args['password']
        reset_token = request.args['reset_token']

        if not reset_token or not password:
            response = jsonify("Token or password error")
            response.headers.add('Access-Control-Allow-Origin', '*')
            return response
        
        try:
            public_id = jwt.decode(reset_token, str(app.config['SECRET_KEY']), algorithms=["HS256"])["public_id"]
        except:
            response = jsonify("Token corrupted or not valid")
            response.headers.add('Access-Control-Allow-Origin', '*')
            return response
        
        try:
            user = User.objects.get(public_id=public_id)
        except:
            response = jsonify("User not found")
            response.headers.add('Access-Control-Allow-Origin', '*')
            return response
        password_in = generate_password_hash(password)
        user.modify(password=password_in)
        user.save()
        finish = tokenBlacklist(token=reset_token, logoutDate=datetime.datetime.utcnow())
        finish.save()
        response = jsonify("password cambiata con successo")
        response.headers.add('Access-Control-Allow-Origin', '*')

        return response
