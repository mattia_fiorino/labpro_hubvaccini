import mongoengine as me


class User(me.Document):
    public_id = me.UUIDField(binary=False, unique=True)
    email = me.EmailField(required=True, unique=True)
    password = me.StringField(required=True, unique=False)
    nome = me.StringField()
    cognome = me.StringField()
    cf = me.StringField(required=True, unique=True)
    telefono = me.StringField()
    deleted = me.BooleanField(required=True, default=False)
