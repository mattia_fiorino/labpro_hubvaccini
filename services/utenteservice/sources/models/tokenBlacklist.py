import mongoengine as me


class tokenBlacklist(me.Document):
    token = me.StringField(required=True, unique=True)
    logoutDate = me.DateTimeField(required=True)
