from flask import Flask


def register_routes(api, app):

    from sources.controllers import register_registerController
    register_registerController(api, app)

    from sources.controllers import register_loginController
    register_loginController(api, app)

    from sources.controllers import register_unusedController
    register_unusedController(api, app)

    from sources.controllers import register_appuntamentiController
    register_appuntamentiController(api, app)

    from sources.controllers import register_reset_requestController
    register_reset_requestController(api, app)

    from sources.controllers import register_resetController
    register_resetController(api, app)
